import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface ResponseModel {
  data: ContactDetails[];
  error: any;
}

interface ContactDetails {
  id: number;
  fullName: string;
  phoneNumber: string;
  addressBookName: string;
}

class UpdatedContact {
  constructor(
    public name: string,
    public phoneNumber: string,
    public addressBookName: string
  ) {}
}

@Injectable({
  providedIn: 'root',
})
export class ShowContactsService {
  addressBookList = [];
  updatedContact = new UpdatedContact('', '', '')
  constructor(private http: HttpClient) {}

  getAllAddressBookNames() {
    return this.http.get<ResponseModel>(
      'http://localhost:8080/contact-address-book/v1/fetch-all-address-books'
    );
  }

  getAllContactsByAddressBookName(addressBookName: string) {
    return this.http.post<ResponseModel>(
      'http://localhost:8080/contact-address-book/v1/fetch-contacts-from-address-book',
      { addressBookName: addressBookName }
    );
  }

  getUniqueContacts() {
    return this.http.get<ResponseModel>(
      'http://localhost:8080/contact-address-book/v1/fetch-unique-contacts'
    );
  }

  addContact(newContact: {
    fullName: string;
    phoneNumber: string;
    addressBookName: string;
  }) {
    return this.http.post(
      'http://localhost:8080/contact-address-book/v1/create-contact',
      newContact
    );
  }

  updateContact(updatedContact: {
    id: number,
    fullName: string;
    phoneNumber: string;
    addressBookName: string;
  }) {
    return this.http.post(
      'http://localhost:8080/contact-address-book/v1/update-contact',
      updatedContact
    );
  }

  deleteContact(id: number){
    return this.http.delete('http://localhost:8080/contact-address-book/v1/delete-contact/' + id)
  }
}
