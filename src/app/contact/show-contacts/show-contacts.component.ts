import { Component, ElementRef, OnInit } from '@angular/core';
import { ShowContactsService } from '../show-contacts.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-show-contacts',
  templateUrl: './show-contacts.component.html',
  styleUrl: './show-contacts.component.scss',
})
export class ShowContactsComponent implements OnInit {
  isContactDetailsPresent: boolean = false;
  contactDetailsList: any[] | undefined;
  addressBookList: any[] | undefined;
  isUniqueContacts = false;
  isUpdateMode: boolean[] = [];
  noContactsFoundMessage = ''

  constructor(private showContactsService: ShowContactsService) {}

  ngOnInit() {
    this.showContactsService.getAllAddressBookNames().subscribe((response) => {
      console.log(response.data);
      this.addressBookList = response.data;
      this.addressBookList.push('Unique Contacts');
      for(let i=0; i< response.data.length; i++){
        this.isUpdateMode.push(false)
      }
      this.noContactsFoundMessage = 'No contact details found!!! Click any address book first'
    });
  }

  onShowContacts(addressBookName: string) {
    if (addressBookName === 'Unique Contacts') {
      this.showContactsService.getUniqueContacts().subscribe((response) => {
        this.isContactDetailsPresent = true;
        this.isUniqueContacts = true;
        this.contactDetailsList = response.data;
        if(response.data.length === 0){
          this.isContactDetailsPresent = false
          this.noContactsFoundMessage = 'No contact details found in ' + addressBookName + ' adddress book'
        }
      });
    } else {
      this.showContactsService
        .getAllContactsByAddressBookName(addressBookName)
        .subscribe((response) => {
          this.isContactDetailsPresent = true;
          this.isUniqueContacts = false;
          this.contactDetailsList = response.data;
          if(response.data.length === 0){
            this.isContactDetailsPresent = false
            this.noContactsFoundMessage = 'No contact details found in ' + addressBookName + ' adddress book'

          }
        });
    }
  }

  onClickUpdate(
    index: number,
    contactId: number,
    contactName: string,
    contactNumber: string,
    contactAddressBook: string
  ) {
    if (this.isUpdateMode[index]) {
      let updatedContact = {
        id: contactId,
        fullName: contactName,
        phoneNumber: contactNumber,
        addressBookName: contactAddressBook,
      };
      this.showContactsService
        .updateContact(updatedContact)
        .subscribe((response) => {
          alert('Contact Updated Successfully');
          this.isUpdateMode[index] = false;
        });
    } else {
      this.isUpdateMode[index] = true;
    }
  }

  onDelete(contactId: number, contactAddressBook: string){
    this.showContactsService.deleteContact(contactId).subscribe(response => {
      alert('Contact Deleted Successfully')
      this.onShowContacts(contactAddressBook)
    })
  }
}
