import { Component, OnInit } from '@angular/core';
import { ShowContactsService } from '../show-contacts.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrl: './add-contact.component.scss'
})
export class AddContactComponent implements OnInit{

  addressBookList: any[] | undefined
  addContactForm!: FormGroup;
  constructor(private showContactsService: ShowContactsService){}
  

  ngOnInit(){
    this.showContactsService.getAllAddressBookNames().subscribe(response => {
      console.log(response.data)
      this.addressBookList = response.data
    })

    this.initForm()
    
}

onSubmit(){
  let newContact = {
    fullName: this.addContactForm.get('nameInput')?.value,
    phoneNumber: this.addContactForm.get('phoneNumberInput')?.value,
    addressBookName: this.addContactForm.get('addressBookInput')?.value,
  }
  this.showContactsService.addContact(newContact).subscribe(Response=>{
    alert('Contact Details Saved Successfully')
    this.addContactForm.reset()
  })
}

private initForm(){
  let contactName= ''
  let contactNumber = ''
  let contactAddressBook = ''
  this.addContactForm = new FormGroup({
    nameInput: new FormControl(contactName, Validators.required),
    phoneNumberInput: new FormControl(contactNumber, Validators.required),
    addressBookInput: new FormControl(contactAddressBook, Validators.required)
  })
}

}
