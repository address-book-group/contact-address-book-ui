import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ShowContactsService } from '../show-contacts.service';

@Component({
  selector: 'app-update-contact',
  templateUrl: './update-contact.component.html',
  styleUrl: './update-contact.component.scss'
})
export class UpdateContactComponent {

  updateContactForm: FormGroup = new FormGroup({
    nameInput: new FormControl(''),
    phoneNumberInput : new FormControl(''),
    addressBookInput : new FormControl('')
  })
  constructor(private showContactService: ShowContactsService){
   
  }
  ngOnInit(){
    console.log('Init')
    this.initForm()
    console.log('End')
  }

  private initForm(){
    
  }
  onSubmit(){

  }
}
