import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAddressBookComponent } from './show-address-book.component';

describe('ShowAddressBookComponent', () => {
  let component: ShowAddressBookComponent;
  let fixture: ComponentFixture<ShowAddressBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowAddressBookComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShowAddressBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
