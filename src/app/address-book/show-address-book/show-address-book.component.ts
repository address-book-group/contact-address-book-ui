import { Component } from '@angular/core';
import { ShowContactsService } from '../../contact/show-contacts.service';
import { AddressBookService } from '../address-book.service';

@Component({
  selector: 'app-show-address-book',
  templateUrl: './show-address-book.component.html',
  styleUrl: './show-address-book.component.scss'
})
export class ShowAddressBookComponent {

  addressBookList: any[] = []

  constructor(private showContactsService: ShowContactsService, private addressBookService: AddressBookService){}
  ngOnInit(){
    
    this.getAllAddressBookNames()
    }

    private getAllAddressBookNames(){
      this.showContactsService.getAllAddressBookNames().subscribe(response => {
        this.addressBookList = response.data
      })
    }
    onDelete(addressBookName: string){
      console.log(addressBookName)
      this.addressBookService.deleteAddressBook(addressBookName).subscribe(response => {
        this.getAllAddressBookNames()
      })
    }
  }

