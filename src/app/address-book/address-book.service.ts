import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddressBookService {

  constructor(private http: HttpClient) { }

  addNewAddressBook(newAddressBook: {addressBookName: string}){
    return this.http.post('http://localhost:8080/contact-address-book/v1/create-address-book', newAddressBook)
  }

  deleteAddressBook(addressBookName: string){
    return this.http.delete('http://localhost:8080/contact-address-book/v1/delete-address-book/' + addressBookName)
  }
}
