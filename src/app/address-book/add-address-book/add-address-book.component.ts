import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddressBookService } from '../address-book.service';

@Component({
  selector: 'app-add-address-book',
  templateUrl: './add-address-book.component.html',
  styleUrl: './add-address-book.component.scss'
})
export class AddAddressBookComponent {

  addAddressBookForm: FormGroup
  constructor(private addressBookService: AddressBookService){}

  ngOnInit(){
    this.initForm()
    
  }

  private initForm(){
    this.addAddressBookForm = new FormGroup({
      addressBookInput: new FormControl('', Validators.required)
    })
  }

  onSubmit(){
    let newAddressBook = {addressBookName: this.addAddressBookForm.get('addressBookInput')?.value}
    this.addressBookService.addNewAddressBook(newAddressBook).subscribe(response=> {
      alert('New AddressBook Added Successfully')
      this.addAddressBookForm.reset()
    }, error =>{
      alert(error.error.errors.errorMessage)
    });
    
  }
}
