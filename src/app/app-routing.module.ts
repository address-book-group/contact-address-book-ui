import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowContactsComponent } from './contact/show-contacts/show-contacts.component';
import { AddContactComponent } from './contact/add-contact/add-contact.component';
import { AddAddressBookComponent } from './address-book/add-address-book/add-address-book.component';
import { ShowAddressBookComponent } from './address-book/show-address-book/show-address-book.component';
import { UpdateContactComponent } from './contact/update-contact/update-contact.component';

const routes: Routes = [
  {path: '', redirectTo: '/show-addressbook', pathMatch: 'full'},
  {path: 'show-contacts', component: ShowContactsComponent},
  {path: 'add-contacts', component: AddContactComponent},
  {path: 'add-addressbook', component: AddAddressBookComponent},
  {path: 'show-addressbook', component: ShowAddressBookComponent},
  {path: 'update-contact', component: UpdateContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
