import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ShowContactsComponent } from './contact/show-contacts/show-contacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AddContactComponent } from './contact/add-contact/add-contact.component';
import { AddAddressBookComponent } from './address-book/add-address-book/add-address-book.component';
import { ShowAddressBookComponent } from './address-book/show-address-book/show-address-book.component';
import { UpdateContactComponent } from './contact/update-contact/update-contact.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ShowContactsComponent,
    AddContactComponent,
    AddAddressBookComponent,
    ShowAddressBookComponent,
    UpdateContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
