
# Project Title

This project contains the UI code of the address book project. 


## Installation

Clone the project

```bash
  git clone https://gitlab.com/address-book-group/contact-address-book-ui.git
```

Versions used

```bash
  npm 9.5.1
  node 18.16.1
  angular 17.0.5
```

Install dependencies

```bash
  npm i
```

Run the application

```bash
  ng s
```
    